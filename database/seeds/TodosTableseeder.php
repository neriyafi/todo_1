<?php

use Illuminate\Database\Seeder;

class TodosTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('todos')->insert(
        [
            [
                'title' => 'Buy milk',
                'user_id' => 1,
                'created_at' => date('Y,m,d G:i:s'),
            ],
            [
                'title' => 'Prepare for the test',
                'user_id' => 1,
                'created_at' => date('Y,m,d G:i:s'),
            ],
            [
                'title' => 'Read a book',
                'user_id' => 2,
                'created_at' => date('Y,m,d G:i:s'),
            ],
        ]); 

        DB::table('books')->insert(
            [
                [
                    'title' => 'Oliver Twist',
                    'author' => 'Charles Dickens' ,
                    'created_at' => date('Y,m,d G:i:s'),
                ],
                [
                    'title' => 'Romeo and Juliet',
                    'author' => 'William Shakespear' ,
                    'created_at' => date('Y,m,d G:i:s'),
                ],
            ]);
    }
}
